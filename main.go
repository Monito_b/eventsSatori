/*Package eventsSatori implements a siple library to handle the http request for films events.
  It uses gorilla tool kit to the api   structure

The rest api for manage Events of films:

    eventSatori:
*/
package main

import (
	"log"
	"net/http"
)

func main() {

	router := NewRouter()
	log.Fatal(http.ListenAndServe(":8080", router))
}
